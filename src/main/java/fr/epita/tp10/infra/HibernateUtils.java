package fr.epita.tp10.infra;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	
	private static SessionFactory sessionfactory;
	
	
	
	public static SessionFactory getSessionFactory() {
		if(sessionfactory==null) {
			
			sessionfactory=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}
		return sessionfactory;
	}

}
