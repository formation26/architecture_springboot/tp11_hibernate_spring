package fr.epita.tp10.infra;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import fr.epita.tp10.domaine.Produit;

@Repository
public class DaoProduitImpl implements DaoProduit {

	@Override
	public List<Produit> findAllProducts() {
		
		SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
		
		Session session =sessionfactory.openSession();
		Query query=session.createQuery("select p from Produit p JOIN FETCH p.categorie");
		List<Produit> produits=query.getResultList();
		session.close();
		return produits;
	}

	@Override
	public List<Produit> findAllExpensiveProduct(double limitPrix) {
       SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
		
		Session session =sessionfactory.openSession();
		Query query=session.createQuery("select p from Produit p where p.prixTTC > :filtrePrix");
		query.setParameter("filtrePrix", limitPrix);
		List<Produit> produits=query.getResultList();
		session.close();
		return produits;
	}

	@Override
	public Produit getProduct(int id) {
       SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
	   Session session =sessionfactory.openSession();
	   Produit p =session.find(Produit.class, id);
	   session.close();
	   return p;
	}

	@Override
	public void create(Produit p) {
		SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
		Session session =sessionfactory.openSession();
        session.save(p);
        session.close();
	}

	@Override
	public void remove(Produit p) {
		SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
		   Session session =sessionfactory.openSession();
		   session.beginTransaction();
		   session.delete(p);
		   session.getTransaction().commit();
		   session.close();

	}

	@Override
	public void update(Produit p) {
		SessionFactory sessionfactory=HibernateUtils.getSessionFactory();
		Session session =sessionfactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(p);
		session.getTransaction().commit();
	    session.close();

	}

}
