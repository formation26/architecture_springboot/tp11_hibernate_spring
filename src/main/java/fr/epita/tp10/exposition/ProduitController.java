package fr.epita.tp10.exposition;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp10.application.ProduitService;
import fr.epita.tp10.domaine.Produit;

@RestController
@RequestMapping("/produit")
public class ProduitController {
	
	@Autowired
	ProduitService service;
	
	@GetMapping("/all")
    List<Produit> findAllProducts(){
		
    	return service.findAllProducts();
    }
	
    @GetMapping("/PriceLimit/{limitPrix}")
	List<Produit> findAllExpensiveProduct(@PathVariable("limitPrix") double limitPrix){
		return service.findAllExpensiveProduct(limitPrix);
	}
	
    @GetMapping("/{id}")
	Produit getProduct(@PathVariable("id") int id) {
		return service.getProduct(id);
	}
	
	@PostMapping
	void create(@RequestBody Produit p) {
		service.create(p);
	}
	
	@DeleteMapping
	void remove(@RequestBody Produit p) {
		service.remove(p);
	}
	
	@PutMapping
	void update(@RequestBody Produit p) {
		service.update(p);
	}

}
